#import "ViewController.h"

@interface ViewController (UIScrollViewDelegate)

@end

@implementation ViewController

@synthesize numberOfElementsInString, numberOfAllElements;
@synthesize cashImage = _cashImage;
@synthesize totalCountTextField, rowCountTextField;
@synthesize loadImage = _loadImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.rowCountTextField.delegate = self;
    self.totalCountTextField.delegate = self;
    scroll.maximumZoomScale = 1.0;
    scroll.minimumZoomScale = 1.0;
    scroll.clipsToBounds = YES;
    scroll.scrollEnabled = YES;
    scroll.delegate = self;
    self.view.userInteractionEnabled = YES;
    scroll.userInteractionEnabled = YES;

    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action: @selector(handleSingleTap:)];
    [scroll addGestureRecognizer:singleTap];    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == rowCountTextField) {
        [rowCountTextField resignFirstResponder];
        [self showGrid:Nil];
        [totalCountTextField becomeFirstResponder];
    } else if (textField == totalCountTextField) {
        [totalCountTextField resignFirstResponder];
        [self showGrid:Nil];
    }
    return YES;
}



-(IBAction)showGrid:(id)sender{

    [self.view endEditing:YES];
    
    for (UIView *subview in scroll.subviews) {
        [subview removeFromSuperview];
        }
    
    self.numberOfElementsInString = self.rowCountTextField.text;
    self.numberOfAllElements = self.totalCountTextField.text;
    
    int screenWidth = 320;

    intNumberOfElementsInString= [numberOfElementsInString intValue];
    intNumberOfAllElements= [numberOfAllElements intValue];
    
    
    if (intNumberOfElementsInString < 13){
        
            if (intNumberOfAllElements<1) {
                intNumberOfAllElements = intNumberOfElementsInString;
                }
            
            
            self.rowCountTextField.delegate = self;
            self.totalCountTextField.delegate = self;
            
            CGFloat intervalWidth = screenWidth / pow((intNumberOfElementsInString+1), 2);
            CGFloat imageWidth = screenWidth / (intNumberOfElementsInString+1);
            locatePointY=locatePointX=intervalWidth;
            
            if (intNumberOfElementsInString==1) {
                intervalWidth = 0;
                imageWidth = 320;
                locatePointY=locatePointX=0;
                }
            
            UIImage * loadImage = [UIImage imageNamed:@"image.png"];
            for  (int currentElementNumber=0; currentElementNumber<intNumberOfAllElements; currentElementNumber++) {
                
                    UIImageView *cashImage = [[UIImageView alloc] initWithImage:loadImage];
                    if (currentElementNumber%intNumberOfElementsInString==0 && currentElementNumber>1) {
                        locatePointY+=(imageWidth+intervalWidth);
                        locatePointX=intervalWidth;
                    }
                
            cashImage.frame = CGRectMake( locatePointX , locatePointY , imageWidth , imageWidth );
            [scroll addSubview:cashImage];
            locatePointX+=(imageWidth+intervalWidth);
            }
            
            frame =(locatePointY+(imageWidth+intervalWidth));
            scroll.contentSize = CGSizeMake(_cashImage.frame.size.width , frame );
            intNumberOfAllElements=0;
    } else {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Warning" message: @"Too many items in a row"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}


- (void)handleSingleTap:(UIGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

-(void)dealloc{
    [scroll release];
    [_cashImage release];
    [_loadImage release];
    [totalCountTextField release];
    [rowCountTextField release];
    [numberOfElementsInString release];
    [numberOfAllElements release];
    [super dealloc];
}
@end
