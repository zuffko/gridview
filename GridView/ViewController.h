#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate>{
    
    IBOutlet UIScrollView     *scroll;
    NSString                  *numberOfElementsInString;
    NSString                  *numberOfAllElements;
    float                 locatePointX;
    float                 locatePointY;
    float                 frame;
    int                   intNumberOfElementsInString;
    int                   intNumberOfAllElements;
}

@property (nonatomic, retain) UIImageView *cashImage;
@property (nonatomic, retain) IBOutlet UITextField *rowCountTextField;
@property (nonatomic, retain) IBOutlet UITextField *totalCountTextField;
@property (nonatomic, retain) UIImage *loadImage;
@property (nonatomic, retain) NSString *numberOfElementsInString;
@property (nonatomic, retain) NSString *numberOfAllElements;

- (IBAction)showGrid:(id)sender;

@end
